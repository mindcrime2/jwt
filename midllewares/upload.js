const path = require('path')
const multer = require('multer')

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
      const uniqueSuffix = path.extname(file.originalname)
      cb(null, Date.now()+uniqueSuffix)
    }
  })
  
  const upload = multer(
    {
        storage: storage,
       
        limits:{
            fileSize:1024*1024*2
        }
    
    })

    module.exports  = upload