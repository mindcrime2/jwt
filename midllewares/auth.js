const jwt = require('jsonwebtoken')


exports.validateToken = (req,res,next)=>{
    try {
        
        const token = req.headers.authorization.split(" ")[1]
        const validate = jwt.verify(token, process.env.JWT_SECRET) 
        req.user = validate   
            next()
        
        
    } catch (error) {
        console.log(error)
       res.status(401).json({status:false,msg:'Token is not valid', error})
    }
}

exports.validateRefreshToken = (req,res,next)=>{
    const {refreshToken} = req.body
    if(!refreshToken){
        res.status(401).json({status:false,msg:'Refresh token is not valid'})
    }
    try {
        const validate = jwt.verify(refreshToken, process.env.JWT_REFRESH) 
        req.user = validate
        console.log(validate)
        const token = jwt.sign({id:validate.id},process.env.JWT_SECRET,{expiresIn:process.env.JWT_TOKEN_TIME})
        
        res.status(200).json({
            msg:'Token refeshed success',
            token,
            refreshToken
        })
            next()
        
        
    } catch (error) {
        console.log(error)
       res.status(401).json({status:false,msg:'Token is not valid', error})
    }
}