const mongoose = require('mongoose')
exports.db = async()=>{
 
try {
  await mongoose.connect(process.env.MONGO_DB,()=>console.log('connected to DB...'));
} catch (error) {
  console.log(error);
}
}