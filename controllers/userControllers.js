
const jwt = require('jsonwebtoken')
const User = require('../models/user')


exports.registerUser = async(req,res)=>{
    
    
    try {
        const {username,password} = req.body
const existingUser = await User.findOne({username})
        if(!username || !password){
            res.status(400).json({status:false,msg:'Userame or passrowd is missing...'})
            return
        }
        if(existingUser){
            res.status(400).json({status:false,msg:'Username already taken...'})
            return
        }
        const user = await User.create({username,password})
        res.status(200).json({status:true,msg:'User registered!',user})
    } catch (error) {
        console.log(error)
        res.status(400).json({status:false,msg:'User registration failed...',error})
    }
}


exports.loginUser = async(req,res)=>{
    const {username,password} = req.body
    if(!username || !password){
        res.status(400).json({status:false,msg:'Something went wrong...'})
    } 
    try {
        const user = await User.findOne({username})
        if(!user){
            res.status(400).json({status:false,msg:'User doesnt exists...'})
        }
        if(await user.comparePwd(password)){

            const token = await user.generateToken()
            const refreshToken = await user.generateRefreshToken()
            res.status(200).json({status:true,msg:`Login success - ${user.username}`, token,refreshToken})
            
        }else{
            res.status(401).json({status:false,msg:'Wrong password...'})    
        }
    } catch (error) {
        console.log(error)
        res.status(401).json({status:false,msg:'Login failed...'})
    }

}



exports.dashboardRoute = async(req,res)=>{
    res.send('Hello from dashboard...')
    
    
}

