const Employee = require('../models/employee')


exports.allEmployees = async(req,res)=>{
    
    try {
        if(req.query.page && req.query.limit){
            const employees = await Employee.paginate({},{page:req.query.page,limit:req.query.limit})
            res.status(200).json({status:true, employees})  
        }
        const employees = await Employee.find()
        res.status(200).json({status:true, employees})  
    } catch (error) {
        res.status(404).json({status:false,msg:'No employees found...'})
    }

}

exports.employeeById = async(req,res)=>{
    try {
        const employee = await Employee.findById(req.params.id)
        res.status(200).json({status:true, employee})  
    } catch (error) {
        res.status(404).json({status:false,msg:`No employee with ID:${req.params.id} found...`})
    }

}

exports.addEmployee = async(req,res)=>{
    
    try {
        let employee = new Employee({
            name:req.body.name,
            designation:req.body.designation,
            email:req.body.email,
            phone:req.body.email,
            age:req.body.age
        })
        console.log(req.files)
        if(req.files){
           let path = ''
           req.files.forEach((file,index) => {
              path += file.path + ','  
           });
           path = path.substring(0, path.lastIndexOf(','))
           employee.avatar = path
          
        }
        await employee.save()
        res.status(200).json({status:true, msg:'New employee added!',employee})  
    } catch (error) {
        console.log(error)
        res.status(404).json({status:false,msg:'An employee could not be added...'})
    }

}

exports.updateEmployee = async(req,res)=>{
    try {
        console.log(req.params.id)
     
        const updatedEmployee = await Employee.findOneAndUpdate(req.params.id,{name:req.body.name})
        res.status(200).json({status:true, msg:'Employee has been updated!',updatedEmployee})  
    } catch (error) {
        console.log(error)
        res.status(404).json({status:false,msg:'An employee could not be updated...'})
    }
}

exports.deleteEmployeeById = async(req,res)=>{
    try {
        await Employee.findOneAndDelete(req.params.id)
        res.status(200).json({status:true, msg:'Deleted'})  
    } catch (error) {
        res.status(404).json({status:false,msg:`Cound not delete employee with ID:${req.params.id}`})
    }

}
