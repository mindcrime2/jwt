const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const User = new mongoose.Schema({
    username:{
        type:String,
        required:true,
        min:6,
        max:255
    },
    password:{
        type:String,
        required:true
    }
})

User.pre('save', async function(){
    const salt = await bcrypt.genSalt()
    this.password = await bcrypt.hash(this.password, salt)
})

User.methods.comparePwd = function(password){
    return bcrypt.compare(password, this.password)
}

User.methods.generateToken = function(){
    return jwt.sign({id:this._id},process.env.JWT_SECRET,{expiresIn:process.env.JWT_TOKEN_TIME})
}

User.methods.generateRefreshToken = function(){
    return jwt.sign({id:this._id},process.env.JWT_REFRESH,{expiresIn:process.env.JWT_REFRESH_TIME})
}


module.exports = mongoose.model('user', User)