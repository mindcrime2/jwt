const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const Employee = new mongoose.Schema({
    name:{
        type:String,
        unique:true,
        required:true
    },
    designation:{
        type:String,
        required:true
    },
    email:{
        type:String,
        unique:true,
        required:true
    },
    phone:{
        type:String,
        required:true
    },
    age:{
        type:Number,
        required:true
    },
    avatar:{
        type:String
    }
},
{
    timestamps:true
})

Employee.plugin(mongoosePaginate)

module.exports = mongoose.model('employee', Employee)