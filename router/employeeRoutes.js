const router = require('express').Router()
const {allEmployees,employeeById,addEmployee,updateEmployee,deleteEmployeeById} = require('../controllers/employeeControllers')
const {validateToken} = require('../midllewares/auth')
const upload = require('../midllewares/upload')

router
    .route('/all')
    .get(validateToken,allEmployees)

router
    .route('/:id')
    .get(employeeById)    
    .patch(updateEmployee)
    .delete(deleteEmployeeById)
router
    .route('/new')
    .post(upload.array('avatar[]'),addEmployee)


    module.exports = router