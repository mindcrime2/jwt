const router = require('express').Router()
const {loginUser,dashboardRoute,registerUser} = require('../controllers/userControllers')
const {validateToken, validateRefreshToken} =require('../midllewares/auth')

router
    .route('/login')
    .post(loginUser)

router
    .route('/register')
    .post(registerUser)

router
    .route('/dashboard')
    .get(validateToken,dashboardRoute)

router
    .route('/token')
    .get(validateRefreshToken)    

module.exports = router