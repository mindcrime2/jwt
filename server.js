const express = require('express')
const app = express()
const cors = require('cors')
const {db} = require('./mongodb')

require('dotenv/config')

db()

app.use(cors())
app.use(express.json())

app.use('/auth',require('./router/userRoutes'))
app.use('/emp',require('./router/employeeRoutes'))

app.listen(process.env.PORT || 3000, ()=>console.log(`listening...${process.env.PORT}`))
